XNAT XferBatch
==============

The [XNAT XferBatch script](https://bitbucket.org/xnatx/xferbatch) provides a means to import large data sets into
XNAT in an easy-to-configure and efficient manner. The script engine is written in Groovy and invoked from a basic
bash script:

```bash
./xferbatch
```

### Command-Line Parameters

**xferbatch** has three mandatory command-line parameters:

* **-a** specifies the root-level URL of the destination XNAT service
* **-u** specifies the user name for the account on the destination XNAT service
* **-w** specifies the password for the account on the destination XNAT service

**xferbatch** also supports two optional command-line parameters:

* **-p** specifies the project on the destination XNAT service to which the data should be assigned
* **-t** specifies the number of concurrent threads to be used when sending the data (the default is 4)

After the command-line parameters, your **xferbatch** command should indicate one or more zip archives containing 
data to be sent to the XNAT server.

A sample command would look something like this:

```bash
$ ./xferbatch -a http://xnat.myschool.edu -u admin -w admin -p XNAT_01 -t 8 data/dicom1.zip data/dicom2.zip data/dicom3.zip
```

### Feature Wish List

**xferbatch** will support more functionality in the future, including:

* The ability to upload folders instead of just archives
* The ability to specify extra metadata about imported sessions
* Better progress tracking and display information for monitoring large data transfer operations
* Optional graphical displays of transfer operations
* Error detection and recovery

