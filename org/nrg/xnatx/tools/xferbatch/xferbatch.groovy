@Grapes([@Grab('org.codehaus.groovy.modules.http-builder:http-builder:0.7.2'),
        @Grab('org.apache.httpcomponents:httpcore:4.2.5'),
        @Grab('org.apache.httpcomponents:httpmime:4.2.5')])

package org.nrg.xnatx.tools.xferbatch

import groovyx.gpars.GParsPool
import groovyx.net.http.HTTPBuilder
import org.apache.http.entity.mime.MultipartEntity
import org.apache.http.entity.mime.content.FileBody
import org.apache.http.entity.mime.content.StringBody

import java.nio.file.Paths

import static groovyx.net.http.Method.POST

def Utils utils = new Utils(args)
def options, valid
(options, valid) = utils.processArgs()
if (!options) {
    return
}

def project = options.p ?: null
def int threads = options.t ? Integer.parseInt((String) options.t) : 4

println "Uploading ${valid.size()} files to ${options.a}" + (project ? " in the project ${project}" : "")

GParsPool.withPool(threads) {
    valid.eachParallel { location, timestamp ->
        def HTTPBuilder xnat = utils.getXnat()
        println "Starting upload of ${location}"
        xnat.get(path: '/servlet/AjaxServlet',
                query: ['remote-class' : 'org.nrg.xnat.ajax.UploadProgress',
                        'remote-method': 'start',
                        'ID'           : "${timestamp}"]) { response ->
        }

        def MultipartEntity entity = new MultipartEntity()
        ['prearchive_code': 0.toString(),
         'auto-archive'   : 'false',
         'quarantine'     : 'false'].each { key, value ->
            entity.addPart(key, new StringBody(value))
        }
        if (project) {
            entity.addPart('project', new StringBody(project))
        }

        def path = Paths.get((String) location)
        def fileName = path.fileName.toString()
        entity.addPart('image_archive', new FileBody(path.toFile(), fileName, 'application/zip'))

        xnat.request(POST) { request ->
            uri.path = '/data/services/import'
            uri.query = ['http-session-listener': "${timestamp}", format: 'html']
            requestContentType = 'multipart/form-data'
            request.entity = entity
            response.success = { resp ->
                println "The file ${location} was successfully uploaded"
            }
            response.failure = { resp ->
                println "The file ${location} failed to upload properly. Error code was: ${resp.status}"
            }
        }
    }
}
