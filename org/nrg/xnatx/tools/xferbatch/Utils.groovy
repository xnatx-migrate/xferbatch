package org.nrg.xnatx.tools.xferbatch

import groovyx.net.http.HTTPBuilder

import java.nio.file.Files
import java.nio.file.Paths

public class Utils {
    def options
    def missing = []
    def valid = [:]

    def Utils(def args) {
        def cli = new CliBuilder(usage: 'xferbatch')
        cli.a(longOpt: 'address', args: 1, argName: 'address', required: true, 'Address of the XNAT server to which the data should be sent.')
        cli.p(longOpt: 'project', args: 1, argName: 'project', required: false, 'Indicates the project to which the data should be assigned.')
        cli.t(longOpt: 'threads', args: 1, argName: 'threads', required: false, 'Indicates the number of concurrent threads to used for uploading. The default is 4.')
        cli.u(longOpt: 'username', args: 1, argName: 'username', required: true, 'Username for the XNAT server.')
        cli.w(longOpt: 'password', args: 1, argName: 'password', required: true, 'Password for the XNAT server.')
        options = cli.parse(args)
    }

    def processArgs() {
        if (!options) {
            println 'There\'s a problem with your command.'
            return [null, null]
        }
        if (!options.arguments() || options.arguments().size() == 0) {
            println('You must specify one or more files or directories containing data to be sent to the XNAT server.')
            return [null, null]
        }

        options.arguments().each { def String location ->
            def path = Paths.get(location)
            if (Files.exists(path)) {
                valid[location] = UUID.randomUUID().toString()
            } else {
                missing << location
            }
        }
        if (missing.size() > 0) {
            println "The following files or directories could not be resolved:\n"
            missing.each {
                println " * ${it}"
            }
            if (valid.size() > 0) {
                print "Would you like to continue to send the remaining ${valid.size()} files to ${options.a}?"
                def input = new BufferedReader(new InputStreamReader(System.in)).readLine()
                if (!(input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes"))) {
                    return [null, null]
                }
            } else {
                return [null, null]
            }
        }
        [options, valid]
    }

    def HTTPBuilder getXnat() {
        def HTTPBuilder xnat = context.get();
        if (!xnat) {
            xnat = new HTTPBuilder(options.a)
            def encoded = "${options.u}:${options.w}".bytes.encodeBase64()
            xnat.headers['Authorization'] = "Basic ${encoded}"
            xnat.get(path: '/data/auth') { response ->
                if (response.status >= 400) {
                    println "Unacceptable response during authentication: ${response.statusLine}"
                    return
                }
            }
            context.set(xnat)
        }
        xnat
    }

    private static final ThreadLocal<HTTPBuilder> context = new ThreadLocal<HTTPBuilder>();
}
